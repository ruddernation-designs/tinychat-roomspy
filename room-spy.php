<?php
/*
* Plugin Name: TinyChat Room Spy 
* Plugin URI: https://github.com/Ruddernation-Designs/tinychat-roomspy/
* Author: Ruddernation Designs
* Author URI: https://github.com/Ruddernation-Designs/
* Description: Allows you to check who is in a TinyChat room and who is on Video/Audio.
* Requires at least: WordPress 4.0
* Tested up to: WordPress 4.5
* Version: 1.4.9
* License: GPLv3
* License URI: http://www.gnu.org/licenses/gpl-3.0.html
* Date: 15th April 2016
*/
define('COMPARE_VERSION', '1.4.8');
register_activation_hook(__FILE__, 'room_spy_install');
function room_spy_install() {
	global $wpdb, $wp_version;
	$post_date = date("Y-m-d H:i:s");
	$post_date_gmt = gmdate("Y-m-d H:i:s");
	$sql = "SELECT * FROM ".$wpdb->posts." WHERE post_content LIKE '%[room_spy_page]%' AND `post_type` NOT IN('revision') LIMIT 1";
	$page = $wpdb->get_row($sql, ARRAY_A);
	if($page == NULL) {
		$sql ="INSERT INTO ".$wpdb->posts."(
			post_author, post_date, post_date_gmt, post_content, post_content_filtered, post_title, post_excerpt,  post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_parent, menu_order, post_type)
			VALUES
			('1', '$post_date', '$post_date_gmt', '[room_spy_page]', '', 'roomspy', '', 'publish', 'closed', 'closed', '', 'roomspy', '', '', '$post_date', '$post_date_gmt', '0', '0', 'page')";
		$wpdb->query($sql);
		$post_id = $wpdb->insert_id;
		$wpdb->query("UPDATE $wpdb->posts SET guid = '" . get_permalink($post_id) . "' WHERE ID = '$post_id'");
	} else {
		$post_id = $page['ID'];
	}
	update_option('room_spy_url', get_permalink($post_id));
}
add_filter('the_content', 'wp_show_room_spy_page', 52);
function wp_show_room_spy_page($content = '') {
	if(preg_match("/\[room_spy_page\]/",$content)) {
		wp_show_room_spy();
		return "";
	}
	return $content;
}
function wp_show_room_spy() {
	if(!get_option('room_spy_enabled', 0)) {
	}
	?>
    <style>#chat,.chatimages{margin:4px 6px 15px;width:220px;height:200px;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;-ms-transition:all .3s ease;transition:all .3s ease;display:inline-block;text-decoration:none;font-size:14px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}input[type=text]{width:25%;}.entry-content img,img[class*=wp-image-]{display:inline-block !important;height:200px;}.password{color:#F00;text-decoration:none}</style>
<?php
$room = filter_input(INPUT_POST, 'room');
if (($room === 'room'));
elseif(preg_match('/^[a-z0-9]/', $room=strtolower($room),$room=strip_tags($room))){
$room=preg_replace('/[^a-zA-Z0-9]/', '',$room);
if (strlen($room) < 3)
$room = substr_replace($room, 'monachechat4', 0);
if (strlen($room) > 36)
$room = substr($room, 0, 36);
$data=file_get_contents('https://api.tinychat.com/'.$room.'.xml');
$rooms=new SimpleXMLElement($data,libxml_use_internal_errors(true));
$data2=file_get_contents('https://tinychat.com/api/tcinfo?username='.$room.'&format=xml');
$room2=new SimpleXMLElement($data2,libxml_use_internal_errors(true));
$array=json_decode(json_encode((array)simplexml_load_string($xml)),1);
}?>
<form method="post"><input type="text" tabindex="1" name="room" title="Enter Tinychat Room." placeholder="Room Name"/> <input type="submit" value="Spy"/></form>
<?php
if (preg_match('/^[a-z0-9]/',$room=strtolower($room), $room=strip_tags($room)))
{
$room=preg_replace('/[^a-zA-Z0-9]/','',$room);
if($room!=='room')
echo '<br/><p>';{
	$pic='https://upload.tinychat.com/pic/'.$room.'';
	$picture='<a href="http://www.ruddernation.info/'.$room.'" title="'.$room.'" target="_blank">
	<img src="'.$pic.'"class="chatimages"></a>';
	echo $picture;}
	echo '<br/>'.$room2['name'].'';
	echo '<br/>'.$room2['biography'].'';
	if($room!==''){
		echo '<br/>
		Statistics: Admins: '.$rooms['mod_count'].' - 
		Chatters: '.$rooms['total_count'].' - 
		On Cam: '.$rooms['broadcaster_count'].'
		<a class="password" title="Password is required to enter the room"/>'.$rooms['error'].'</a></p>';}
		foreach($rooms->names as $names){
			echo '<div id="chat">'.$names.'';
			$pic='https://upload.tinychat.com/i/'.$room.'-'.$names.'.jpg';
			$picture='<a href="http://www.ruddernation.info/'.$room.'" title="Hey! I&#39;m '.$names.', Click to come and chat with me." target="_blank"><img src="'.$pic.'"class="chatimages"></a><br/></div>';
			echo $picture ;
}
	}
		}?>
